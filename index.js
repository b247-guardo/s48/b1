// Event listener to the dropdown menu on the first question
const answerDropdown = document.getElementById("question-1-answer");
const messageElement = document.getElementById("message-1");

answerDropdown.addEventListener("change", selectedOption);

function selectedOption (e) {

	if (answerDropdown.value === "Wolf") {
		messageElement.innerHTML = "Your answer is: <b>Correct!</b>";
	} else {
		messageElement.innerHTML = "Your answer is: <b>Incorrect, please try again.</b>";
	};
};

// Event listener to the dropdown menu on the second question
const secondDropdown = document.getElementById("question-2-answer");
const secondMessage = document.getElementById("message-2");

secondDropdown.addEventListener("change", correctOption);

function correctOption (e) {

	if (secondDropdown.value === "He turned into a boy") {
		secondMessage.innerHTML = "Your answer is: <b>Correct!</b>";
	} else {
		secondMessage.innerHTML = "Your answer is: <b>Incorrect, please try again.</b>";
	};
};

// Event listener to the submit button
const form = document.getElementById("quiz-form");
const submit = document.getElementById("submit-btn");

submit.addEventListener("click", showResults);

function showResults (e) {

	e.preventDefault();
	let firstName = document.getElementById("txt-first-name").value;
	let lastName = document.getElementById("txt-last-name").value;
	let correctAnswers = 0;

	if (document.getElementById("question-1-answer").value === "Wolf") {
		correctAnswers++;
	} 
	if (document.getElementById("question-2-answer").value === "He turned into a boy") {
		correctAnswers++;
	}

	alert(`Good day, ${firstName} ${lastName}. You have ${correctAnswers}/2 questions correct.`);
};